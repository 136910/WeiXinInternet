﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WxInternet.Models
{
    public class ApiCommonResult<T>
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }

    public class ComparerHelper : IEqualityComparer<NavItem>
    {
        public bool Equals(NavItem x, NavItem y)
        {
            return x.Type == y.Type;
        }
        public int GetHashCode(NavItem obj)
        {
            return obj.NavName.GetHashCode();
        }
    }
}