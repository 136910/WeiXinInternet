﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WxInternet.Models
{
    public class HomeManagerModel
    {
        public List<WeiXinUserInfoModel> Level2List { get; set; }
        public List<WeiXinUserInfoModel> RandomJpGzhList { get; set; }
        public Dictionary<List<NavItem>, List<ArticleListItem>> ArticleList { get; set; }
    }
    public class NavItem
    {
        public int Id { get; set; }
        public string NavName { get; set; }
        public int Type { get; set; }
        public int Soft { get; set; }

    }
    public class ArticleListItem
    {
        public int Id { get; set; }
        /// <summary>
        /// 文章图 可能有三张，用$分割
        /// </summary>
        public string ImgUrl { get; set; }
        /// <summary>
        /// 文章标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 文章创建时间//视频的服务器创建时间
        /// </summary>
        public string LastCommentCreateTime { get; set; }
        /// <summary>
        /// 文章描述
        /// </summary>
        public string Abstract { get; set; }
        public int IsVideo { get; set; }
        public string ReadCount { get; set; }
        public int CommentCount { get; set; }
        public int DataType { get; set; }
        public string Note_Class { get; set; }
        public string DataTypeName { get; set; }
    }
    public class WeiXinUserInfoModel
    {
        public int Id { get; set; }
        public string WxName { get; set; }
        public int WxNavId { get; set; }
        public int AreaId { get; set; }
        public int CityId { get; set; }
        public int ProvinceId { get; set; }

        public string WxIntroduce { get; set; }
        /// <summary>
        /// 微信二维码
        /// </summary>
        public string WxQrCode { get; set; }
        /// <summary>
        /// 微信头像图片
        /// </summary>
        public string WxImgUrl { get; set; }
        /// <summary>
        /// 微信号
        /// </summary>
        public string WxCode { get; set; }
        public string SmallIntroduce { get; set; }

        public string CreateTime { get; set; }

        public int InfoStatus { get; set; }

        public int ReadCount { get; set; }

        public int UserId { get; set; }

        public string RefuseReson { get; set; }
        /// <summary>
        /// 支付级别，1免费，2级别一，3级别二
        /// </summary>
        public int PayLevel { get; set; }
        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime ExpiredTime { get; set; }

        public string SuccessTime { get; set; }

    }
}