﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WxInternet.Models;

namespace WxInternet.BussinessData
{
    public class BusinessDataManager
    {
        /// <summary>
        /// 获取级别二的列表
        /// </summary>
        /// <returns></returns>
        public List<WeiXinUserInfoModel> Level2List()
        {
            var queryList = new List<WeiXinUserInfoModel>();
            var sql = string.Format("select cw.Id,cw.WxName,cw.WxQrCode,cw.SmallIntroduce,cw.ReadCount from C_WxOrder co join C_WxUserInfo cw on co.IsPublic=1 and co.PayLevel =3 and co.ExpiredTime>=GETDATE() and co.WxUserInfoId = cw.Id and cw.InfoStatus=1");
            try
            {
                DataTable dt = new SqlManager("ConnectionStrings_WeiXin").ExecuteDataset(CommandType.Text, sql).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        var WeiXinUserInfoModel = new WeiXinUserInfoModel();
                        WeiXinUserInfoModel.Id = Convert.ToInt32(dt.Rows[j]["Id"]);
                        WeiXinUserInfoModel.WxName = dt.Rows[j]["WxName"].ToString();
                        WeiXinUserInfoModel.WxQrCode = dt.Rows[j]["WxQrCode"].ToString();
                        WeiXinUserInfoModel.SmallIntroduce = dt.Rows[j]["SmallIntroduce"].ToString();
                        WeiXinUserInfoModel.ReadCount = Convert.ToInt32(dt.Rows[j]["ReadCount"].ToString());
                        queryList.Add(WeiXinUserInfoModel);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return queryList;
        }

        public List<NavItem> GetNavItemList()
        {
            var sql = "select * from C_Nav where IsPublc = 1";
            var queryList = new List<NavItem>();
            try
            {
                DataTable dt = new SqlManager().ExecuteDataset(CommandType.Text, sql).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        var item = new NavItem();
                        item.Id = Convert.ToInt32(dt.Rows[i]["Id"]);
                        item.NavName = dt.Rows[i]["NavName"].ToString();
                        item.Type = Convert.ToInt32(dt.Rows[i]["Type"]);
                        item.Soft = Convert.ToInt32(dt.Rows[i]["Soft"]);
                        queryList.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return queryList;
        }

        /// <summary>
        /// 随机获取精品微信公众号，付费的
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public List<WeiXinUserInfoModel> RandomJpGzhList(int count)
        {
            var queryList = new List<WeiXinUserInfoModel>();
            var sql = string.Format("select top({0})cw.Id,WxName,ReadCount,WxQrCode from C_WxUserInfo cw left join c_WxOrder co on cw.Id=co.WxUserInfoId and co.ExpiredTime>=GETDATE() where cw.InfoStatus=1 and co.PayLevel>1 order by NEWID()", count);
            try
            {
                DataTable dt = new SqlManager("ConnectionStrings_WeiXin").ExecuteDataset(CommandType.Text, sql).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        var WeiXinUserInfoModel = new WeiXinUserInfoModel();
                        WeiXinUserInfoModel.Id = Convert.ToInt32(dt.Rows[j]["Id"]);
                        WeiXinUserInfoModel.WxName = dt.Rows[j]["WxName"].ToString();
                        WeiXinUserInfoModel.ReadCount = Convert.ToInt32(dt.Rows[j]["ReadCount"]);
                        WeiXinUserInfoModel.WxQrCode = dt.Rows[j]["WxQrCode"].ToString();
                        queryList.Add(WeiXinUserInfoModel);
                    }
                }
            }
            catch (Exception ex)
            {
               
            }
            return queryList;
        }

        /// <summary>
        /// 每个类型获取count条文章最新
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public List<ArticleListItem> GetArticleByPartition(int count)
        {
            var query = new List<ArticleListItem>();
            var sql = "select * from  (select ROW_NUMBER() over(partition by DataType order by Id desc) num ,Id,Title,ImgUrl,DataType from C_Article where IsPublic=1 and ImgUrl<>'') tt where num<=" + count;
            try
            {
                DataTable dt = new SqlManager().ExecuteDataset(CommandType.Text, sql).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        var articleListItem = new ArticleListItem();
                        articleListItem.Id = Convert.ToInt32(dt.Rows[j]["Id"]);
                        articleListItem.ImgUrl = dt.Rows[j]["ImgUrl"].ToString();
                        articleListItem.Title = dt.Rows[j]["Title"].ToString();
                        articleListItem.DataType = Convert.ToInt32(dt.Rows[j]["DataType"]);
                        query.Add(articleListItem);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return query;
        }
    }
}