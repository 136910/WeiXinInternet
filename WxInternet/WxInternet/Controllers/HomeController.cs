﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WxInternet.BussinessData;
using WxInternet.Common;
using WxInternet.Models;

namespace WxInternet.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            var model = GetHomeManagerModel();
            return View(model);
        }

        private HomeManagerModel GetHomeManagerModel()
        {
            var result = new HomeManagerModel();
            result.Level2List = GetLeve21List();
            result.RandomJpGzhList = RandomJpGzhList();
            result.ArticleList = ArticleList();
            return result;
        }
        private Dictionary<List<NavItem>, List<ArticleListItem>> ArticleList()
        {
            var query = CacheHelper.GetCache<Dictionary<List<NavItem>, List<ArticleListItem>>>("WxInternetArticleList");
            if (query == null)
            {
                try
                {
                    var respinseModelList = new BusinessDataManager().GetArticleByPartition(3);
                    if (respinseModelList.Count > 0)
                    {
                        query = new Dictionary<List<NavItem>, List<ArticleListItem>>();
                        //开始分组处理
                        var navs = new BusinessDataManager().GetNavItemList().Where(p => p.Type != 0);
                        query.Add(navs.Where(i => i.Type <= 4).ToList(), respinseModelList.Where(i => i.DataType <= 4).ToList());
                        query.Add(navs.Where(i => i.Type > 4 && i.Type <= 8).ToList(), respinseModelList.Where(i => i.DataType > 4 && i.DataType <= 8).ToList());
                        query.Add(navs.Where(i => i.Type > 8 && i.Type <= 12).ToList(), respinseModelList.Where(i => i.DataType > 8 && i.DataType <= 12).ToList());
                        query.Add(navs.Where(i => i.Type > 12 && i.Type <= 16).ToList(), respinseModelList.Where(i => i.DataType > 12 && i.DataType <= 16).ToList());
                        CacheHelper.SetCache("WxInternetArticleList", query, 60 * 3);
                    }

                }
                catch
                {
                    return new Dictionary<List<NavItem>, List<ArticleListItem>>();
                }
            }
            return query;
        }

        private List<WeiXinUserInfoModel> RandomJpGzhList()
        {
            var query = CacheHelper.GetCache<List<WeiXinUserInfoModel>>("WxInternetRandomJpGzhList");
            if (query == null)
            {
                query = new BusinessDataManager().RandomJpGzhList(7);
                try
                {
                    if (query.Count > 0)
                    {
                        CacheHelper.SetCache("WxInternetRandomJpGzhList", query, 60 * 4);
                    }
                }
                catch
                {
                    return new List<WeiXinUserInfoModel>();
                }
            }
            return query;
        }

        private List<WeiXinUserInfoModel> GetLeve21List()
        {
            var query = CacheHelper.GetCache<List<WeiXinUserInfoModel>>("WEBGetLevel1List");
            if (query == null)
            {
                query = new BusinessDataManager().Level2List();
                try
                {
                    if (query.Count > 0)
                    {
                        CacheHelper.SetCache("WEBGetLevel1List", query, 60 * 4);
                    }
                }
                catch
                {
                    return new List<WeiXinUserInfoModel>();
                }
            }
            return query;
        }
    }
}
