﻿
(function () {
    nestPageClass = new NestPageClass();
    function NestPageClass() {
        var _self = this;

        function _init() {
            _self.IsShowAndUpdateButtonForPage = IsShowAndUpdateButtonForPage;
        }
        function IsShowAndUpdateButtonForPage(pagingResult) {
            var readyCount = pagingResult.PageIndex * pagingResult.PageSize + pagingResult.RecordCount;

            if (readyCount >= pagingResult.TotalCount || pagingResult.TotalCount==0) {
                $("#nextPage").remove();
            } else {
                if ($("#fyButton").html().trim() != "") {
                    
                    var nestIndex = parseInt(pagingResult.PageIndex) + 1;
                    $("#nextPage").attr("NextPageIndex", nestIndex.toString());
                } else {
                    $("#fyButton").html('<ul>  <li class="prev-page"></li><li class="next-page"><a id="nextPage" PageSize="' + pagingResult.PageSize + '" NextPageIndex="' + (parseInt(pagingResult.PageIndex) + 1) + '" href="javascript:void(0);">加载更多</a></li></ul>');
                }
               
            }
        }
        _init();
    }
})();
